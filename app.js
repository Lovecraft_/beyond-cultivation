const express = require('express'),
      path = require('path'),
      cookieParser = require('cookie-parser'),
      logger = require('morgan'),
      journalDb = require('better-sqlite3')('journal.db', null),
      postGrowDb = require('better-sqlite3')('postGrow.db', null),
      // routers
      postYourGrowRouter = require('./routes/discord/postyourgrow'),
      discordRouter = require('./routes/discord/discordclient'),
      webhooksRouter = require('./routes/discord/webhooks'),
      analyticsRouter = require('./routes/analytics'),
      postsRouter = require('./routes/posts'),
      journalRouter = require('./routes/journals'),
      socialsRouter = require('./routes/socials'),
      backendRouter = require('./routes/backend'),
      app = express();
// Setup Express server
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public'))); //Static Pages
// Routes
app.use('/postyourgrow', postYourGrowRouter);
app.use('/discord', discordRouter);
app.use('/discord/webhooks', webhooksRouter);
app.use('/analytics', analyticsRouter);
app.use('/posts', postsRouter);
app.use('/journals', journalRouter);
app.use('/socials', socialsRouter);
app.use('/backend', backendRouter);
module.exports = app;
// Setup discord tie-in
require('./services/discord/discordBackend');