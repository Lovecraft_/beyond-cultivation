var express = require('express'),
    router = express.Router(),
    discordClient = require('../../services/discord/discordBackend');

/* GET */
router.get('/', (req, res, next) => {
  console.log(discordClient)
  res.send({discordClient});
});

module.exports = router;