var express = require('express'),
    router = express.Router(),
    tree = require('object-treeify'),
    fs = require('fs'),
    files = fs.readdirSync('./routes' );

/* GET */
router.get('/', (req, res, next) => {
  res.setHeader('Content-Type', 'text/html')
  res.send(`${(tree(files, true, false).replace(/(?:\r\n|\r|\n)/g, '<br />').replace(/(?:.js)/g, ''))}`);
});

module.exports = router;